# Docker Proxy Container

A simple Docker Container based on Alpine Linux and Squid

## Changelog

- 28/05/2024
    - Rebuild with new versions
    - Pin Alpine to 3.20
- 31/01/2024
    - Rebuild with new versions
    - Pin Alpine to 3.19
- 20/07/2023
    - Rebuild with new versions
    - Pin Alpine to 3.18
- 20/03/2023
    - Rebuild with new versions
- 04/07/2022
    - Rebuild with new versions
- 30/04/2021
    - Rebuild with new versions
