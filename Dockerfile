FROM alpine:3.20
LABEL org.opencontainers.image.authors="Stéphane Ludwig <gitlab@stephane-ludwig.net>"

RUN apk add --no-cache --update squid

CMD ["/usr/sbin/squid", "-N", "-Y", "-C", "-d 1"]
